/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WS;

import Controlador.CrearXmlController;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author WCARO
 */
@WebService(serviceName = "SistemaXml")
public class SistemaXml {

    
     CrearXmlController dat = new CrearXmlController();
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "GenerarCarpetaFactura")
    public boolean GenerarCarpetaFactura(@WebParam(name = "num") int num) {
        
        boolean datos = dat.GenerarCarpetaFactura(num);
        return datos;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "GeneraCarpetaNotaCredito")
    public int GeneraCarpetaNotaCredito(@WebParam(name = "numeroarchivos") int numeroarchivos) {
        //TODO write your implementation code here:
        return  numeroarchivos;
    }

    /**
     * Web service operation
     */
   
}
